﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Serialization;
using aspnetcore11api001.Configuration;

namespace aspnetcore11api001
{
    /// <summary>
    /// The main start-up class for the application.
    /// </summary>
    public class Startup
    {

        #region Fields
        private readonly IConfigurationRoot configuration;
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly int? sslPort;
        #endregion

        public Startup(IHostingEnvironment hostingEnvironment)
        {
            this.hostingEnvironment = hostingEnvironment;
            this.configuration = new ConfigurationBuilder()
                .SetBasePath(this.hostingEnvironment.ContentRootPath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{this.hostingEnvironment.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables()
                .AddApplicationInsightsSettings(developerMode: !this.hostingEnvironment.IsProduction())
                .Build();

            if (this.hostingEnvironment.IsDevelopment())
            {

            }
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddApplicationInsightsTelemetry(this.configuration)
                .AddOptions(this.configuration)
                .AddRouting(options =>
                {
                    options.LowercaseUrls = true;
                })
                .ConfigureCorsPolicies()
                .AddMvc(options =>
                {
                    options.Conventions.Add(new FeaturesConvention());
                })
                .AddRazorOptions(options => 
                {
                    options.ViewLocationFormats.Insert(0, "/Features/Shared/{0}.cshtml");
                    options.ViewLocationFormats.Insert(0, "/Features/{3}/{0}.cshtml");
                    options.ViewLocationFormats.Insert(0, "/Features/{3}/{1}/{0}.cshtml");

                    options.AreaViewLocationFormats.Insert(0, "/Areas/{2}/Features/Shared/{0}.cshtml");
                    options.AreaViewLocationFormats.Insert(0, "/Areas/{2}/Features/{3}/{0}.cshtml");
                    options.AreaViewLocationFormats.Insert(0, "/Areas/{2}/Features/{3}/{1}/{0}.cshtml");
                })
                .AddJsonOptions(x =>
                {
                    x.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                })
                .Services.AddcompositionRoot();
        }


        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (hostingEnvironment.IsDevelopment())
            {
                loggerFactory
                    .AddConsole()
                    .AddDebug();
            }

            app.UseCors("AllowAny");
            if (hostingEnvironment.IsDevelopment())
            {

            }
            app.UseMvc();
        }
    }
}
