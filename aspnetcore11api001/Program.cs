﻿using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace aspnetcore11api001
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel(options => 
                {
                    options.AddServerHeader = false;
                    options.UseHttps("cert.pfx", "Maelstrom120");
                })
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseAzureAppServices()
                .UseIISIntegration()
                .UseStartup<Startup>()
                .UseApplicationInsights()
                .Build();

            host.Run();
        }
    }
}
