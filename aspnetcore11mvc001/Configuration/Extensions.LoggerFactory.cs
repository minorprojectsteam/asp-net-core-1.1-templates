﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace aspnetcore11mvc001.Configuration
{
    public static partial class Extensions
    {
        public static ILoggerFactory UseIf(this ILoggerFactory loggerFactory, bool predicate,
            Action<ILoggerFactory> action)
        {
            if (predicate)
            {
                action(loggerFactory);
            }
            return loggerFactory;
        }
    }
}
