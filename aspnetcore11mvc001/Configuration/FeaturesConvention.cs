﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Razor;

namespace aspnetcore11mvc001.Configuration
{
    public class FeaturesConvention : IControllerModelConvention
    {
        private const string Features = "features";

        public void Apply(ControllerModel controller)
        {
            controller.Properties.Add(Features, GetFeatureName(controller.ControllerType.FullName));
        }

        private static string GetFeatureName(string controllerFullName)
        {
            var tokens = controllerFullName.Split('.');

            if (tokens.All(t => t.ToLower() != Features))
            {
                return string.Empty;
            }

            return tokens
                .SkipWhile(t => !t.Equals(Features, StringComparison.CurrentCultureIgnoreCase))
                .Skip(1)
                .Take(1)
                .FirstOrDefault();
        }
    }

    public class FeaturesViewConvention : IViewLocationExpander
    {
        private const string Features = "features";

        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (viewLocations == null)
            {
                throw new ArgumentNullException(nameof(viewLocations));
            }

            var controllerActionDescriptor = context.ActionContext.ActionDescriptor as ControllerActionDescriptor;

            if (controllerActionDescriptor == null)
            {
                throw new NullReferenceException("ControllerActionDescriptor cannot be null.");
            }

            var featureName = controllerActionDescriptor.Properties[Features] as string;
            foreach (var location in viewLocations)
            {
                yield return location.Replace("{3}", featureName);
            }
        }

        public void PopulateValues(ViewLocationExpanderContext context) {}
    }
}
