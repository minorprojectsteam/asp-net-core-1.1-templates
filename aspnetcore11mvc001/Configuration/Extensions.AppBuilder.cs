﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;

namespace aspnetcore11mvc001.Configuration
{
    public static partial class Extensions
    {
        public static IApplicationBuilder UseIf(this IApplicationBuilder app, bool predicate,
            Action<IApplicationBuilder> action)
        {
            if (predicate)
            {
                action(app);
            }
            return app;
        }

        public static IApplicationBuilder UseIfElse(this IApplicationBuilder app, bool predicate,
            Action<IApplicationBuilder> ifAction, Action<IApplicationBuilder> elseAction)
        {
            if (predicate)
            {
                ifAction(app);
            }
            else
            {
                elseAction(app);
            }
            return app;
        }
    }
}
