﻿namespace aspnetcore11mvc001.Configuration
{
    using Microsoft.AspNetCore.Mvc.Razor;

    public static partial class Extensions
    {
        /// <summary>
        /// Adds ability to use FeaturesConvention with Default MVC Convention for Views 
        /// </summary>
        /// <param name="options"></param>
        public static void AddFeaturesWithDefaultConvention(this RazorViewEngineOptions options)
        {
            // {0} - Action Name
            // {1} - Controller Name
            // {2} - Area Name
            // {3} - Feature Name

            // add support for features side-by-side with /Views
            // (do NOT clear ViewLocationFormats)
            options.ViewLocationFormats.Insert(0, "/Features/Shared/{0}.cshtml");
            options.ViewLocationFormats.Insert(0, "/Features/{3}/{0}.cshtml");
            options.ViewLocationFormats.Insert(0, "/Features/{3}/{1}/{0}.cshtml");

            //(do NOT clear AreaViewLocationFormats)
            options.AreaViewLocationFormats.Insert(0, "/Areas/{2}/Features/Shared/{0}.cshtml");
            options.AreaViewLocationFormats.Insert(0, "/Areas/{2}/Features/{3}/{0}.cshtml");
            options.AreaViewLocationFormats.Insert(0, "/Areas/{2}/Features/{3}/{1}/{0}.cshtml");

            options.ViewLocationExpanders.Add(new FeaturesViewConvention());
        }
    }
}
