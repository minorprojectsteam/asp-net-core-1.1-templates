﻿using aspnetcore11mvc001.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Serialization;

namespace aspnetcore11mvc001
{
    public class Startup
    {

        #region Fields
        private readonly IConfigurationRoot _configuration;
        private readonly IHostingEnvironment _environment;
        #endregion

        public Startup(IHostingEnvironment env)
        {
            _environment = env;

            _configuration = new ConfigurationBuilder()
                .SetBasePath(_environment.ContentRootPath)
                .AddJsonFile("appsettings.json", reloadOnChange: true, optional: false)
                .AddJsonFile($"appsettings.{_environment.EnvironmentName}.json", reloadOnChange: true, optional: true)
                .AddEnvironmentVariables()
                .AddApplicationInsightsSettings(!_environment.IsProduction())
                .Build();

            if (_environment.IsDevelopment())
            {

            }
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddApplicationInsightsTelemetry(_configuration)        // configure application insights
                .AddOptions(_configuration)                             // configure options - configuration classes
                .AddRouting(options =>                                  // configure routing
                {
                    options.LowercaseUrls = true;
                })
                .AddCorsPolicies()                                      // configure and register Cors policies
                .AddMvc(options =>                                      // configure ASP NET MVC 
                {
                    options.Conventions.Add(new FeaturesConvention());
                })
                .AddRazorOptions(options =>                             // configure Razor View Engine
                {
                    options.AddFeaturesWithDefaultConvention();
                })
                .AddJsonOptions(x =>                                    // configure JSON (de)serialization
                {
                    x.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                })
                .Services
                .AddCompositionRoot();                                  // add custom classes/services to IoC
        }


        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.UseIf(env.IsDevelopment(), factory =>
            {
                factory
                    .AddConsole()
                    .AddDebug();

            });

            app.UseCors("AllowAny");

            app.UseIf(env.IsDevelopment(), a =>
            {
                a.UseDeveloperExceptionPage();
            });

            app.UseMvc();
        }
    }
}
