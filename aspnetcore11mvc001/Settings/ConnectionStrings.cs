﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace aspnetcore11mvc001.Settings
{
    public class ConnectionStrings
    {
        public string DefaultConnection { get; set; }
    }
}
